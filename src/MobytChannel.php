<?php

namespace MadBob\LaravelNotification\Mobyt;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Events\NotificationFailed;

class MobytChannel
{
    protected $config;
    protected $current_userkey;
    protected $current_sessionkey;
    protected $last_call;

    public function __construct()
    {
        $this->config = (object) [
            'driver' => config('services.mobyt.driver'),
            'username' => config('services.mobyt.username'),
            'password' => config('services.mobyt.password')
        ];

        $this->current_userkey = '';
        $this->current_sessionkey = '';
        $this->last_call = '';
    }

    private function endpoint($driver, $type)
    {
        $default_obj = (object) [
            'login' => sprintf('https://app.esendex.it/API/v1.0/REST/login?username=%s&password=%s', $this->config->username, $this->config->password),
            'send' => 'https://app.esendex.it/API/v1.0/REST/sms',
        ];

        $supported = [
            'esendex' => $default_obj,
            'mobyt' => $default_obj,
            'smsitaly' => $default_obj,

            'skebby' => (object) [
                'login' => sprintf('https://api.skebby.it/API/v1.0/REST/login?username=%s&password=%s', $this->config->username, $this->config->password),
                'send' => 'https://api.skebby.it/API/v1.0/REST/sms'
            ]
        ];

        if (isset($supported[$driver]))
            return $supported[$driver]->$type;
        else
            throw new \Exception('Unable to send SMS: invalid API endpoint. Please check your configuration');
    }

    private function authenticate()
    {
        /*
            Access token expires after 5 minutes, here we ask for a new one
            after 4 minutes (just in case)
        */
        if (empty($this->last_call) || $this->last_call < (time() - (60 * 4))) {
            $client = new Client();
            $endpoint = $this->endpoint($this->config->driver, 'login');

            $request = new Request('GET', $endpoint);
            $response = $client->send($request, [
                'timeout' => 2,
                'http_errors' => false,
            ]);

            if ($response->getStatusCode() != 200) {
                throw new \Exception('Failed authentication to SMS gateway: ' . $response->getReasonPhrase());
            }

            $data = explode(';', $response->getBody()->getContents());
            $this->current_userkey = $data[0];
            $this->current_sessionkey = $data[1];
        }

        $this->last_call = time();
    }

    public function send($notifiable, Notification $notification)
    {
        try {
            $this->authenticate();

            $message = $notification->toMobyt($notifiable);

            $to = $this->getTo($notifiable);
            if (is_array($to)) {
                foreach($to as $t)
                    $message->addRecipient($t);
            }
            else {
                $message->addRecipient($to);
            }

            $client = new Client();

            $headers = [
                'Content-type' => 'application/json',
                'user_key' => $this->current_userkey,
                'Session_key' => $this->current_sessionkey,
            ];

            $body = $message->toJson($this->config->driver);
            $endpoint = $this->endpoint($this->config->driver, 'send');

            $request = new Request('POST', $endpoint, $headers, $body);
            $response = $client->send($request, [
                'timeout' => 2,
                'http_errors' => false,
            ]);

            if ($response->getStatusCode() != 201) {
                throw new \Exception('Failed to send SMS through gateway: ' . $response->getReasonPhrase());
            }
        }
        catch (Exception $exception) {
            $event = new NotificationFailed($notifiable, $notification, 'mobyt', ['message' => $exception->getMessage(), 'exception' => $exception]);
            event($event);
        }
    }

    protected function getTo($notifiable)
    {
        if ($notifiable->routeNotificationFor('mobyt')) {
            return $notifiable->routeNotificationFor('mobyt');
        }

        if (isset($notifiable->phone_number)) {
            return $notifiable->phone_number;
        }

        throw new \Exception('Unable to send SMS: invalid recipient');
    }
}
