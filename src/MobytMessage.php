<?php

namespace MadBob\LaravelNotification\Mobyt;

class MobytMessage
{
    const HI_QUALITY = 'N';
    const MID_QUALITY = 'L';
    const LOW_QUALITY = 'LL';

    private $type;
    private $content;
    private $from;
    private $recipients;

    public function __construct($content = '')
    {
        $this->type = self::MID_QUALITY;
        $this->content = $content;
        $this->from = config('services.mobyt.from');
        $this->recipients = [];
    }

    public function type($type)
    {
        $this->type = $type;
        return $this;
    }

    public function content($content)
    {
        $this->content = $content;
        return $this;
    }

    public function from($from)
    {
        $this->from = $from;
        return $this;
    }

    public function addRecipient($recipient)
    {
        /*
            This is to normalize the number, remove all non-numeric characters
            and format the international prefix in the expected way
        */
        $recipient = preg_replace('/^\+/', '00', $recipient);
        $recipient = preg_replace('/[^0-9]/', '', $recipient);
        $recipient = preg_replace('/^00/', '+', $recipient);

        if (in_array($recipient, $this->recipients) == false)
            $this->recipients[] = $recipient;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function toJson($driver = 'mobyt')
    {
        $obj = (object) [
            'message' => $this->content,
            'recipient' => $this->recipients,
        ];

        if ($this->type != self::LOW_QUALITY && !empty($this->from)) {
            $obj->sender = $this->from;
        }

        /*
            Skebby has different codes to handle message quality,
            here we remap it
        */
        if ($driver == 'skebby') {
            switch($this->type) {
                case self::HI_QUALITY:
                    $this->type = 'GP';
                    break;
                case self::MID_QUALITY:
                    $this->type = 'TI';
                    break;
                case self::LOW_QUALITY:
                    $this->type = 'SI';
                    break;
            }
        }

        $obj->message_type = $this->type;

        return json_encode($obj);
    }
}
