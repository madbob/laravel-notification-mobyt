# Laravel Notification Mobyt

This package integrates the [Esendex SMS Gateway](https://www.esendex.it/servizi/sms/api-sms/) service (formerly known as Mobyt, from which the name of the package) to the native [Laravel Notifications](https://laravel.com/docs/master/notifications) system, so to be able to dispatch your notification with a simple `$user->notify(new YourNotification())`.

The library is compatible with other SMS services using the same API, including:

* [Skebby](https://www.skebby.it/)

## Installation

`composer require madbob/laravel-notification-mobyt`

## Configuration

In your `config/services.php` file, add the following block:

```
'mobyt' => [
    /*
        Identifier of one of the supported SMS services:
        esendex
        skebby
        mobyt // Mobyt no longer exists, this is kept for historical reasons and acts as the "esendex" driver
        smsitaly // SMSItalia no longer exists, this is kept for historical reasons and acts as the "esendex" driver
    */
    'driver' => 'esendex',

    /*
        Custom name to display as "sender"
    */
    'from' => 'YourName',

    /*
        Username for your account
    */
    'username' => env('MOBYT_USERNAME'),

    /*
        Password for your account
    */
    'password' => env('MOBYT_PASSWORD'),
],
```

## Usage

Add a `toMobyt()` function to the Notification you want to deliver via SMS, and add the `MobytChannel` class to the channels involved.

```
use MadBob\LaravelNotification\Mobyt\MobytChannel;
use MadBob\LaravelNotification\Mobyt\MobytMessage;
use Illuminate\Notifications\Notification;

class YourNotification extends Notification
{
    public function via($notifiable)
    {
        return [MobytChannel::class];
    }

    public function toMobyt($notifiable)
    {
        return (new MobytMessage())->content("Your message here!");
    }
}
```

In order to let your notification know which phone number are you sending to, the channel will look for the `phone_number` attribute of the `Notifiable` model. If you want to override this behaviour, add the `routeNotificationForMobyt()` method to your `Notifiable` model.

```
public function routeNotificationForMobyt()
{
    return '+1234567890';
}
```

The `MobytMessage` object has other options, including:

* `type()` to specify the type of message, you can choose among `MobytMessage::HI_QUALITY`, `MobytMessage::MID_QUALITY` and `MobytMessage::LOW_QUALITY`
* `from()` to dynamically change the custom name of sender
* `addRecipient()` to add more recipients numbers to the message. Always remember to add the international prefix!

## License

This code is free software, licensed under the The GNU General Public License version 3 (GPLv3). See the LICENSE.md file for more details.

Copyright (C) 2017 Roberto Guido <bob@linux.it>
